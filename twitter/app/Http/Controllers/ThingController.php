<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ThingController extends Controller
{
    public function index()
    {
        $faker = \Faker\Factory::create();

        return view('things.index', [
            'things' => \App\Thing::all(),
            'faker' => $faker->company
        ]);
    }

    public function create()
    {
        return view('things.create_form');
    }

    function store()
    {
        $validator = \Validator::make($_POST, [
            'name' => 'required|foo'
        ], [
            'foo' => 'Your input is not foo enough.'
        ]);

        if($validator->fails()) {
            return redirect('things/create')
                ->withErrors($validator)
                ->withInput();
        }

        $thing = new \App\Thing();
        $thing->name = $_POST['name'];
        $thing->save();

        return redirect('/things');
    }
}
