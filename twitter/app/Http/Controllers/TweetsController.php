<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TweetsController extends Controller
{
    public function index()
    {
        $loggedInUser = new User;
        $tweets = \App\Tweet::whereUserId(1)->get();
        $tweets = \App\Tweet::where('user_id', 1)->get();

        $user = \App\User::whereEmail('something@example.com')->first();

        return view('index', [
            'tweets' => $tweets,
            'thing' => 'hahaha'
        ]);
    }

    public function create()
    {
        return view('create');
    }

    public function store()
    {
        $validator = \Validator::make($_POST, [
            'tweet' => 'required',
        ],[
            'tweet.regex' => 'You call that valid input!!??',
        ]);

        if($validator->fails()) {
            return redirect('/tweets/create')->withErrors($validator)->withInput();
        } else {

            $tweet = new \App\Tweet;
            $tweet->tweet = $_POST['tweet'];
            $tweet->user_id = 1;
            $tweet->save();

            return redirect('/tweets');
        }
    }
}
