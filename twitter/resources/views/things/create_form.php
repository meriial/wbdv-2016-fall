<?php if(count($errors->all()) > 0): ?>
    <ul class="errors">
        <?php foreach($errors->all() as $error): ?>
            <li><?php echo $error ?></li>
        <?php endforeach ?>
    </ul>
<?php endif; ?>

<form method="post" action="create">
    <?php echo csrf_field() ?>
    <input type="text" name="name" value="">
    <button type="submit" name="button">Submit</button>
</form>
