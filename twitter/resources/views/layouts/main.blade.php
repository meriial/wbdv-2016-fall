<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page</title>
    </head>
    <body>
        <header>
            Header Stuff
        </header>
        @yield('content')
        <footer>Footer Stuff</footer>
    </body>
</html>
