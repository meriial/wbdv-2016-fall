<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tweets/create', 'TweetsController@create');
// Route::post('/tweets/create', 'TweetsController@store');
Route::post('/tweets/storeTweet', 'TweetsController@store');
Route::get('/tweets', 'TweetsController@index');



Route::get('/things', 'ThingController@index');
Route::get('/things/create', 'ThingController@create');
Route::post('/things/create', 'ThingController@store');
