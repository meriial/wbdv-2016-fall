<?php

function redirectTo($url)
{
    header('Location: '.$url);
}

function correctEmail()
{
    return $_POST['email'] == 'correct@email.com';
}

function correctPassword()
{
    return $_POST['password'] == 'correct-password';
}

function formWasSubmitted()
{
    return !empty($_POST);
}

function whateverTheUserEnteredFor($fieldName)
{
    if (formWasSubmitted()) {
        return $_POST[$fieldName];
    }

    return '';
}
