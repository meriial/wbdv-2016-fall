<?php

    require 'src/functions.php';

    if(formWasSubmitted()) {
        redirectTo('success.php?message='.$_POST['item']);
    }

    require 'views/main.php';
