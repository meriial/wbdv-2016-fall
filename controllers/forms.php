<?php

require 'src/functions.php';

$errors = [];

if (theFormWasSubmitted()) {

  $errors = collectAllErrors();

  if (empty($errors)) {
    redirectToSuccessPage();
    // showSuccessMessage();
  }

}

layoutView('views/form.php', [
    'errors' => $errors,
    'title' => 'Form'
]);
