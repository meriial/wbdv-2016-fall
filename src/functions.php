<?php

function layoutView($viewFile, $data) {
    extract($data);

    ob_start();
    require $viewFile;
    $content = ob_get_clean();

    require 'views/layouts/main.php';
}


function redirectToSuccessPage()
{
  header('Location: success.php');
}

function showErrors($errors)
{
  foreach ($errors as $error) {
    echo $error;
  }
}

function collectAllErrors()
{
  $errors = [];

  if(!validField('tweet')) {
    $errors[] = generateError('You must enter a tweet.');
  }

  if(!validField('name')) {
    $errors[] = generateError('You must enter a name.');
  }

  if(tweetIsTooLong()) {
    $errors[] = generateError('Your tweet is too long.');
  }

  return $errors;
}

function theFormWasSubmitted() {
  return !empty($_POST);
}

function validTweet() {
  return !empty($_POST['tweet']);
}

function validName()
{
  return !empty($_POST['name']);
}

function validField($fieldName) {
  return !empty($_POST[$fieldName]);
}

function generateError($error) {
  return '<span style="color:red">'.$error.'</span>';
}

function tweetIsTooLong() {
  return strlen($_POST['tweet']) > 10;
}

function showInput()
{
  var_dump($_POST);
}

function showSuccessMessage() {
  echo 'Your tweet was tweeted.';
}
