<html>
<head>
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen">
</head>
<body>
    <header>
        <h1>My Awesome App</h1>
        <nav>
            <ul class="nav nav-pills">
                <li><a href="other.php">Other</a></li>
                <li><a href="forms.php">Forms</a></li>
            </ul>
        </nav>
    </header>

    <?php echo $content ?>

    <footer>
        Copyright 2016 me
    </footer>
</body>
</html>
