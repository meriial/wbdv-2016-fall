<?php

class Form {

    public $errors = [];

    public function wasSubmitted()
    {
        return !empty($_POST);
    }

    public function fieldWasValid($fieldName)
    {
        return !empty($_POST[$fieldName]);
    }

    public function hasNoErrors()
    {
        if(!$this->fieldWasValid('email')) {
            $this->errors[] = 'bad email';
        }

        return empty($this->errors);
    }

    public function errors()
    {
        return join(',', $this->errors);
    }

}
