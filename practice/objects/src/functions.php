<?php


function formWasSubmitted()
{
    return !empty($_POST);
}

function fieldWasValid($fieldName)
{
    return !empty($_POST[$fieldName]);
}
