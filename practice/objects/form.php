<?php

    require 'src/functions.php';
    require 'src/Form.php';

    $form = new Form();

    if ($form->wasSubmitted()) {
        if($form->hasNoErrors()) {
            // do something
            echo 'success!';
        } else {
            echo $form->errors();
        }
    }
 ?>

<form method="post">
    <input type="text" name="email" />

    <button type="submit">Submit</button>
</form>
