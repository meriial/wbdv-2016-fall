<?php

function formWasSubmitted()
{
  return !empty($_POST);
}

function fieldHasValue($fieldName)
{
  return !empty($_POST[$fieldName]);
}

function noErrors()
{
  return fieldHasValue('field_1');
}

function redirectTo($url)
{
  header('Location: '.$url);
}

if (formWasSubmitted() && noErrors()) {
  redirectTo('afterSubmit.php');
}

?>

<form method="post">
  <?php if(formWasSubmitted() && !fieldHasValue('field_1')){ ?>
    <p>You need to enter a value.</p>
  <?php } ?>
  <input name="field_1" type="text" />
  <button type="submit">Submit</button>
</form>
