<html>
<head>
    <title>Demo</title>
</head>
<body>
    <header>
        <ul>
            <li><a href="form.php">Form</a></li>
            <li><a href="about.php">About</a></li>
        </ul>
    </header>

    <?php echo $content ?>

    <footer>
        Copyright <?php echo date('Y') ?> Me
    </footer>
</body>
