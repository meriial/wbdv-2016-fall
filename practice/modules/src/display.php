<?php

function layoutView($viewName, $data) {

    extract($data);

    ob_start();
    require $viewName;
    $content = ob_get_clean();

    require 'views/layouts/main.php';
}
