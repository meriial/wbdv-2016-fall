<?php

function formWasSubmitted()
{
    return !empty($_POST);
}

function formErrors()
{
    $fields = [
        'field_1'
    ];

    $errors = [];

    foreach ($fields as $field) {
        if( empty($_POST[$field])) {
            $errors[$field] = 'You must fill out this field.';
        }
    }

    return $errors;
}
