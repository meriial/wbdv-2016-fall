<?php

    require 'src/validation.php';
    require 'src/display.php';

    $messages = [];

    if(formWasSubmitted()) {
        $errors = formErrors();
        if(!empty($errors)) {
            $messages = $errors;
        } else {
            $messages[] = 'Thank-you for filling out this form.';
        }
    }

    layoutView('views/form.php', [
        'messages' => $messages,
        'thing' => 'hahaha'
    ]);
