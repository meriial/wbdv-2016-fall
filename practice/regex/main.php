<pre>
<?php

$actualUrl = '/blog/sunday-21';

$routes = [
    'about-us' => 'about.php',
    'forms' => 'forms.php',
    'blog/(.+)/(.+)' => 'blog.php'
];

foreach ($routes as $url => $controller) {
    $pattern = "|^/$url$|";
    $match = preg_match($pattern, $actualUrl, $matches);
    if ($match) {
        if (isset($matches[1])) {
            $id = $matches[1];
            echo 'passing in id of '.$id;
        }

        echo 'found controller '.$controller."\n";
        // require 'controllers/'.$controller;
        break;
    }
}

if (!$match) {
    echo '404 page';
}
