<?php

    function formWasSubmitted()
    {
        return !empty($_POST);
    }

    function redirectTo($url) {
        header('Location: '.$url);
    }

    if(formWasSubmitted()) {
        redirectTo('form_challenge2_redirect.php?message='.$_POST['field_1']);
    }
 ?>

<form method="post">

    <input type="text" name="field_1">

    <button type="submit">Submit</button>

</form>
