<?php

require_once 'src/database.php';

runQuery('DROP TABLE IF EXISTS users');
runQuery('DROP TABLE IF EXISTS tweets');

$createTweets = "CREATE TABLE IF NOT EXISTS `tweets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY( id )
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$createUsers = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `real_name` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";

$updateIndices2 = "
ALTER TABLE `users`
  ADD UNIQUE KEY `unique_handle` (`handle`),
  ADD UNIQUE KEY `unique_email` (`email`);
";

runQuery($createTweets);
runQuery($createUsers);
runQuery($updateIndices2);

echo 'database reset!';
?>
