<?php

function formWasSubmitted()
{
    return !empty($_POST);
}

function runQuery($sql)
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');
    $result = $db->query($sql);

    if ($result == false) {
        echo '<pre>';
        var_dump('VERY BAD ERROR', $db->error);
        die;
    }
}

function select($sql)
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');
    $result = $db->query($sql);

    if ($result == false) {
        echo '<pre>';
        var_dump('VERY BAD ERROR', $db->error);
        die;
    }

    return $result;
}

function getTweets()
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets');

    $tweets = $result->fetch_all(MYSQLI_ASSOC);

    return $tweets;
}

function getTweetsForUserId($userId)
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from tweets WHERE user_id = '.$userId);

    $tweets = $result->fetch_all(MYSQLI_ASSOC);

    return $tweets;
}

function userNameForUserId($userId)
{
    $sql = 'SELECT * FROM users WHERE id = '.$userId;

    $result = select($sql);
    $user = $result->fetch_assoc();

    $userName = $user['handle'];

    return $userName;
}

function saveTweet()
{
    date_default_timezone_set('America/Edmonton');
    $tweetText = $_POST['text'];
    $currentDateTime = date('Y-m-d H:i:s');

    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $tweetText = $db->real_escape_string($tweetText);
    $userId = $db->real_escape_string($_POST['user_id']);

    $sql = "INSERT INTO `twitter`.`tweets` (`id`, `text`, `datetime`, `user_id`) VALUES (NULL, '$tweetText', '$currentDateTime', $userId);";
    $result = runQuery($sql);

}

function saveTweetFramework() {
    $tweet = new Tweet;
    $tweet->text = $_POST['text'];
    $tweet->datetime = date('Y-m-d H:i:s');
    $tweet->save();
}

if (formWasSubmitted()) {
    saveTweet();
}

$tweets = getTweetsForUserId(6);

?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
</head>

<form method="post">
    <input type="text" name="text" placeholder="text"/>
    <input type="number" name="user_id" placeholder="user_id"/>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<table class="table">
<?php foreach ($tweets as $tweet) { ?>
    <tr>
    <td><?= $tweet['text'] ?></td>
    <td><?= $tweet['datetime'] ?></td>
    <td><?= userNameForUserId($tweet['user_id']) ?></td>
    <td><a href="deleteTweet.php?tweetId=<?= $tweet['id'] ?>">delete</a></td>
    </tr>
<?php } ?>
</table>
