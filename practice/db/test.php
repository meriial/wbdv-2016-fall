<?php

function userIsNotLoggedIn()
{
    session_start();

    if (isset($_SESSION['isLoggedIn'])) {
        return false;
    } else {
        return true;
    }
}

if (userIsNotLoggedIn()) {
    header('Location: login.php');
}

?>

<h1>Protected Content!!</h1>
<p>Hello, <?php echo $_SESSION['user']['real_name'] ?></p>
<a href="logout.php">logout</a>
