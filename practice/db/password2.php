<?php

    $errors = [];
    if (formWasSubmitted()) {
        if (passwordIsCorrect()) {
            echo 'You are correct!';
        } else {
            $errors[] = 'Stay out!';
        }
    }

    function formWasSubmitted()
    {
        return !empty($_POST);
    }

    function passwordIsCorrect()
    {
        $realPassword = '1234';
        $hashedPassword = password_hash($realPassword, PASSWORD_DEFAULT);


        $enteredPassword = $_POST['password'];
        $verified = password_verify($enteredPassword, $hashedPassword);

        return $verified;
    }
?>

<?php echo join(',', $errors) ?>
<form method="post">
    <input type="password" name="password" />

    <button type="submit">Submit</button>
</form>
