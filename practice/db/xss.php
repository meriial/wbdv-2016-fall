<?php

    $stuff = '';
    if (formWasSubmitted()) {
        $stuff = $_POST['stuff'];
    }

    function formWasSubmitted() {
        return !empty($_POST);
    }

    function e($string)
    {
        return htmlentities($string);
    }
?>

<form method="post">
    <input type="text" name="stuff" />

    <button type="submit">Submit</button>
</form>

<div class="stuff">
    <?= e($stuff) ?>

    {!! $stuff !!}
</div>
