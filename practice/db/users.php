<?php

function formWasSubmitted()
{
    return !empty($_POST);
}

function getUsers()
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');

    $result = $db->query('SELECT * from users');

    $users = $result->fetch_all(MYSQLI_ASSOC);

    return $users;
}

function saveUser()
{
    date_default_timezone_set('America/Edmonton');
    $handle = $_POST['handle'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $real_name = $_POST['real_name'];
    $currentDateTime = date('Y-m-d H:i:s');

    $sql = "INSERT INTO `twitter`.`users` (`id`, `handle`, `email`, `real_name`, `password` ) VALUES (NULL, '$handle', '$email', '$real_name', '$password');";

    runQuery($sql);
}

function runQuery($sql)
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');
    $result = $db->query($sql);

    if ($result == false) {
        var_dump('VERY BAD ERROR', $db->error);
        die;
    }
}

function select($sql)
{
    $db = new mysqli('localhost', 'sait', '1234', 'twitter');
    $result = $db->query($sql);

    return $result;
}

function fieldIsUnique($tableName, $fieldName)
{
    $fieldValue = $_POST[$fieldName];
    $sql = "SELECT * FROM $tableName WHERE $fieldName = '$fieldValue'";

    $result = select($sql);

    return $result->num_rows == 0;
}

if (formWasSubmitted()) {
    if(fieldIsUnique('users', 'email') && fieldIsUnique('users', 'handle')) {
        saveUser();
    } else {
        echo 'That handle and/or email is already taken, please choose another one.';
    }
}

$users = getUsers();

?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="screen" title="no title">
</head>

<form method="post">
    <input type="text" name="handle" />
    <input type="text" name="email" />
    <input type="text" name="real_name" />
    <input type="text" name="password" />
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<table class="table">
<?php foreach ($users as $user) { ?>
    <tr>
    <td><?= $user['handle'] ?></td>
    <td><?= $user['email'] ?></td>
    <td><?= $user['real_name'] ?></td>
    <td><?= $user['password'] ?></td>

    </tr>
<?php } ?>
</table>
