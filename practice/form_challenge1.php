<?php

    function isChecked($fieldName)
    {
        return isset($_POST[$fieldName]);
    }

    if(isChecked('checkbox_a') && isChecked('checkbox_b')) {
        echo 'ERROR!';
    }

?>

<form method="post">

    <input type="checkbox" name="checkbox_a" value="on" />
    <input type="checkbox" name="checkbox_b" value="on" />

    <button type="submit" name="button">Submit</button>

</form>
