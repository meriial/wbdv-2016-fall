<?php
  class Animal {
    public $sound = 'default sound';

    public function speak() {
      echo 'My sound is '.$this->sound;
    }
  }

  class Dog extends Animal {

    public $sound = 'bow wow';

  }

  class Cat extends Animal {
    public $sound = 'meow';

    public function begForFood()
    {
      echo 'MEOOOOOW!';
    }
  }

  $fido = new Dog;
  $fido->speak();


  $misty = new Cat;
  $misty->speak();
  $misty->begForFood();


?>
