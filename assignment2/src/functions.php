<?php

function formWasSubmitted()
{
    return !empty($_POST);
}

function redirectTo($url)
{
    header('Location: '.$url);
}

function passwordNotComplex()
{
    $password = $_POST['password'];

    $noUppercase = !preg_match('/[A-Z]/', $password);
    $noLowercase = !preg_match('/[a-z]/', $password);
    $noSpecial   = !preg_match('/\W/', $password);

    if ($noUppercase || $noLowercase || $noSpecial ) {
        return true;
    } else {
        return false;
    }
}

function userForEmail($email)
{
    $sql = "SELECT * FROM users WHERE email='$email'";
    $result = runQuery($sql);

    return $result->fetch_assoc();
}

function passwordIsValid($user, $password)
{
    return password_verify($password, $user['password']);
}

function loginWasValid()
{
    $email = $_POST['email'];
    $password = $_POST['password'];

    $user = userForEmail($email);

    if($user && passwordIsValid($user, $password)) {
        return true;
    } else {
        return false;
    }
}

function loginUser()
{
    $email = $_POST['email'];
    $user = userForEmail($email);
    $_SESSION['user'] = $user;
}

function loggedIn()
{
    return isset($_SESSION['user']);
}
