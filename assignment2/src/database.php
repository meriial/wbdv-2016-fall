<?php

function runQuery($sql)
{
    $db = new mysqli('localhost', 'wbdv', '1234', 'wbdv');

    $result = $db->query($sql);

    if(!$result) {
        var_dump('BAD ERROR...', $db->error);
    }

    return $result;
}

function saveUser()
{
    $db = new mysqli('localhost', 'wbdv', '1234', 'wbdv');

    $email = $db->real_escape_string($_POST['email']);
    $password = $db->real_escape_string($_POST['password']);
    $name = $db->real_escape_string($_POST['name']);

    $password = password_hash($password, PASSWORD_DEFAULT);

    $sql = "INSERT INTO `wbdv`.`users` (`id`, `email`, `password`, `name`) VALUES (NULL, '$email', '$password', '$name');";

    runQuery($sql);
}

function saveItem()
{
    $db = new mysqli('localhost', 'wbdv', '1234', 'wbdv');

    $item = $db->real_escape_string($_POST['item']);
    $user_id = $_SESSION['user']['id'];

    $sql = "INSERT INTO `wbdv`.`items` (`id`, `name`, `user_id`) VALUES (NULL, '$item', '$user_id');";

    runQuery($sql);
}


/**
 * Something here.
 */
function getItemsForLoggedInUser()
{
    $user_id = $_SESSION['user']['id'];

    // because of client request, we need to not escape
    // this input
    $sql = "SELECT * FROM items WHERE user_id=$user_id";
    // a good place for a comment.
    $result = runQuery($sql); // not a good place for a comment.

    /**
     * Bad place for docblock comment
     */

    $result = $result->fetch_all(MYSQL_ASSOC);

    return $result;
}
