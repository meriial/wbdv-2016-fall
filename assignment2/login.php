<?php

    session_start();
    
    require_once 'src/functions.php';
    require_once 'src/database.php';

    if(formWasSubmitted()) {
        if (!loginWasValid()) {
            echo 'Bad login.';
        } else {
            loginUser();
            redirectTo('main.php');
        }
    }

?>

<form method="post">
    <input type="text" name="email" value=""/>
    <input type="text" name="password" value="" />
    <button type="submit">Submit</button>
</form>
