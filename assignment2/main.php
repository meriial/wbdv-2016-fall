<?php

    session_start();

    require_once 'src/functions.php';
    require_once 'src/database.php';

    if(!loggedIn()) {
        redirectTo('login.php');
    }

    function itemWasValid()
    {
        return true;
    }

    if(formWasSubmitted()) {
        if (!itemWasValid()) {
            echo 'Bad item.';
        } else {
            saveItem();
            redirectTo('main.php');
        }
    }

    $items = getItemsForLoggedInUser();

?>

Hello, <?php echo $_SESSION['user']['name'] ?>
<form method="post">
    <input type="text" name="item" value=""/>
    <button type="submit">Submit</button>
</form>

<ul>
    <?php foreach ($items as $item) { ?>
        <li><?php echo $item['name'] ?></li>
    <?php } ?>
</ul>
