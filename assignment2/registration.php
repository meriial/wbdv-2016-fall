<?php

    require_once 'src/functions.php';
    require_once 'src/database.php';

    if(formWasSubmitted()) {
        if (passwordNotComplex()) {
            echo 'Your password is not complex enough.';
        } else {
            saveUser();
            redirectTo('login.php');
        }
    }

?>

<form method="post">
    <input type="text" name="email" value=""/>
    <input type="text" name="name" value="" />
    <input type="text" name="password" value="" />
    <button type="submit">Submit</button>
</form>
