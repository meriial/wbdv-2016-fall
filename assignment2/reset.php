<?php

require_once 'src/database.php';

runQuery('DROP TABLE IF EXISTS users');
runQuery('DROP TABLE IF EXISTS tweets');

$createTweets = "CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY( id )
) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

$createUsers = "CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
";

$updateIndices2 = "
ALTER TABLE `users`
  ADD UNIQUE KEY `unique_email` (`email`);
";

runQuery($createTweets);
runQuery($createUsers);
runQuery($updateIndices2);

echo 'database reset!';
?>
