<?php

    require_once 'src/functions.php';

    session_start();

    session_unset();

    session_destroy();

    redirectTo('login.php');
