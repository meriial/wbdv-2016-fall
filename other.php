<?php

require 'src/functions.php';

$things = [
    'one',
    'two'
];

$otherThing = 'bad';

layoutView('views/other.php', [
    'thingies' => $things,
    'title' => 'Other'
]);
